import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_exam/model/element.dart';

part 'guess_element_event.dart';
part 'guess_element_state.dart';

class GuessElementBloc extends Bloc<GuessElementEvent, GuessElementState> {
  GuessElementBloc() : super(const ElementsLoadSuccess(elements: [])) {
    on<AppStarted>(
      (event, emit) {
        _elementsCount = 5 + Random().nextInt(6);
        _specialElementIndex = Random().nextInt(_elementsCount);
        _elementsList = List.generate(
          _elementsCount,
          (index) => Element(index, false, false),
        );
        emit(GuessElementInitial());
        emit(ElementsLoadSuccess(elements: _elementsList));
      },
    );

    on<ElementSelected>(
      (event, emit) async {
        if (event.elementIndex == _specialElementIndex) {
          _elementsList[event.elementIndex] = _elementsList[event.elementIndex]
              .copyWith(isSelected: true, guessedRight: true);

          _changeSpecialElementIndex();
        } else {
          final tempList = _elementsList
              .where((element) => !element.guessedRight && element.isSelected);

          for (var element in tempList) {
            _elementsList[element.index] =
                _elementsList[element.index].copyWith(isSelected: false);
          }

          _elementsList[event.elementIndex] =
              _elementsList[event.elementIndex].copyWith(isSelected: true);

          _changeSpecialElementIndex();
        }

        emit(GuessElementInitial());
        emit(ElementsLoadSuccess(
          elements: _elementsList,
        ));

        bool isEveryElementGuessedRight =
            _elementsList.where((element) => !element.guessedRight).length == 1;

        if (isEveryElementGuessedRight) {
          await Future.delayed(const Duration(milliseconds: 500));

          _elementsList = _elementsList
              .map((element) =>
                  element.copyWith(guessedRight: false, isSelected: false))
              .toList();

          emit(GuessElementInitial());
          emit(ElementsLoadSuccess(
            elements: _elementsList,
          ));
        }
      },
    );
  }

  late int _elementsCount;
  late int _specialElementIndex;
  late List<Element> _elementsList;

  void _changeSpecialElementIndex() {
    final elementsToChooseFrom = _elementsList
        .where((element) => !element.isSelected && !element.guessedRight);

    if (elementsToChooseFrom.isNotEmpty) {
      _specialElementIndex = elementsToChooseFrom
          .elementAt(Random().nextInt(elementsToChooseFrom.length))
          .index;
    }
  }
}
