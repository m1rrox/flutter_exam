part of 'guess_element_bloc.dart';

abstract class GuessElementEvent extends Equatable {
  const GuessElementEvent();

  @override
  List<Object> get props => [];
}

class AppStarted extends GuessElementEvent {
  const AppStarted();
}

class ElementSelected extends GuessElementEvent {
  const ElementSelected({required this.elementIndex});

  final int elementIndex;

  @override
  List<Object> get props => [elementIndex];
}
