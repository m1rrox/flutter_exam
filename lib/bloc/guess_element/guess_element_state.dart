part of 'guess_element_bloc.dart';

abstract class GuessElementState extends Equatable {
  const GuessElementState();

  @override
  List<Object> get props => [];
}

class GuessElementInitial extends GuessElementState {}

class ElementsLoadSuccess extends GuessElementState {
  final List<Element> elements;

  const ElementsLoadSuccess({required this.elements});

  @override
  List<Object> get props => [elements];
}
