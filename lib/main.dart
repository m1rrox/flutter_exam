import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_exam/app.dart';
import 'package:flutter_exam/bloc/guess_element/guess_element_bloc.dart';

void main() {
  runApp(
    BlocProvider<GuessElementBloc>(
      create: (context) => GuessElementBloc(),
      child: const App(),
    ),
  );
}
