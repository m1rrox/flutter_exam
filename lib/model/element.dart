import 'package:equatable/equatable.dart';

class Element extends Equatable {
  final int index;
  final bool guessedRight;
  final bool isSelected;

  Element(this.index, this.guessedRight, this.isSelected);

  Element copyWith({bool? guessedRight, bool? isSelected}) {
    return Element(
      index,
      guessedRight ?? this.guessedRight,
      isSelected ?? this.isSelected,
    );
  }

  @override
  List<Object?> get props => [index, guessedRight, isSelected];
}
