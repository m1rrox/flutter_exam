import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_exam/widget/element_box.dart';
import 'package:flutter_exam/bloc/guess_element/guess_element_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Guess the element'),
      ),
      body: Center(
        child: BlocBuilder<GuessElementBloc, GuessElementState>(
          builder: (context, state) {
            return Column(
              children: (state as ElementsLoadSuccess)
                  .elements
                  .map(
                    (element) => ElementBox(
                      isSelected: element.isSelected,
                      isMainElement: element.guessedRight,
                      title: element.index.isEven ? 'Element 2' : 'Element 1',
                      onPressed: () {
                        if (!element.isSelected) {
                          context.read<GuessElementBloc>().add(
                                ElementSelected(elementIndex: element.index),
                              );
                        }
                      },
                    ),
                  )
                  .toList(),
            );
          },
        ),
      ),
    );
  }
}
