import 'package:flutter/material.dart';

class ElementBox extends StatelessWidget {
  const ElementBox({
    Key? key,
    required this.isSelected,
    required this.isMainElement,
    required this.title,
    required this.onPressed,
  }) : super(key: key);

  final bool isSelected;
  final bool isMainElement;
  final String title;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    final String elementTitle = isSelected && !isMainElement ? '' : title;
    final Color? elementColor =
        isSelected && !isMainElement ? Colors.grey[700] : Colors.green;

    return InkWell(
      onTap: onPressed,
      child: Container(
        padding: const EdgeInsets.only(bottom: 8.0),
        alignment: Alignment.center,
        height: 45.0,
        color: isSelected ? elementColor : Colors.grey[200],
        child: Text(
          elementTitle,
          style: const TextStyle(
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
